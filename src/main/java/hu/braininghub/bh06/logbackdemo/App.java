package hu.braininghub.bh06.logbackdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {

		for (int i = 0; i < 10000; i++) {
			LOGGER.info("This is an info level message");
			LOGGER.debug("This is a debug level message");
			LOGGER.trace("This is a TRACE level message");
			LOGGER.warn("This is a WARN level message");
			LOGGER.error("This is an ERROR message");
		}

	}
}
